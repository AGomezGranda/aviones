/**
 * Copyright 2019 Álvaro Gómez and Gonzalo Paúl Licensed under the Apache License, Version 2.0 (the "License");you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
limitations under the License.
*/

# Catálogo de Aviones

Bienvenido al catálogo de aviones que registrará tus aviones favoritos


## Instalación Jar

Utiliza el siguiente comando para poder utilizar el programa

```
make jar
```

## Uso del Programa

	1. Para mostrar ayuda, utilice el siguiente comando:
```
java -jar aviones.jar show
```

	2. Para añadir tus modelos de aviones a la lista utilice:
```
java -jar aviones.jar add <modelo> <fabricante> <velocidad máxima(km/h)> <alcance (km)>
```

	3. Para ver el catálogo de aviones disponibles utilice:
```
java -jar aviones.jar show
	
```
	4. Para eliminar cualquier avión identificado con una matrícula utilice:
```
java -jar aviones.jar remove <matricula>
```
	5. Para modificar cualquier avión, hágalo mediante la matrícula, utilizando:
```
java -jar aviones.jar modify <matricula> <modelo> <fabricante> <velocidad máxima> <alcance>
```
        6. Para generar la hoja de cálculo:
```
java -jar aviones.jar spreadsheet
```
	7. Para ver la hoja de cálculo generada:
```
java -jar aviones.jar showSpreadsheet
```



# Información para desarrolladores
## Generación de Javadoc

Se ejecuta la siguiete instrucción:

~~~~
make javadoc
~~~~

## Inspección de Javadoc

Suponiendo que tiene instalado `firefox`, se ejecuta:

~~~~
firefox html/index.html
~~~~

