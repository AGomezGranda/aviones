# Copyright 2019 Alvaro Gomez and Gonzalo Paul Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.  

jar:compile
	jar cvfm aviones.jar Manifest.txt -C bin .

compile:clean
	find src -name *.java | xargs javac -cp bin -d bin

javadoc:
	rm -rf html
	mkdir html
	find src -name "*.java" | xargs javadoc -d html -encoding utf-8 -docencoding utf-8 -charset utf-8

clean:
	rm -rf bin
	mkdir bin

