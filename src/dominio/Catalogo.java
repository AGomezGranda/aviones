/**
 * Copyright 2019 Álvaro Gómez and Gonzalo Paúl
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package dominio;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import java.io.FileWriter;
import java.util.Scanner;


public class Catalogo{

	private ArrayList<Avion> listaAviones = new ArrayList<>();
	private static String nombreFichero = "aviones.txt";
	public static String nombreFicheroCSV = "aviones.csv";
	
	public Catalogo(){
		cargarDesdeFichero();
	}

	public void annadirAvion(Avion avion){
		listaAviones.add(avion);
		volcarAFichero();
	}

	public void mostrarAviones()
	{
		for (Avion avion : listaAviones) System.out.println(avion.generarStringParaMostrar());
	}
	
	private void volcarAFichero(){
		try {
			FileWriter fw = new FileWriter(nombreFichero);
			fw.write(this.toString());
			fw.close();
		}catch(IOException ex){
			System.err.println("Error al intentar escribir en fichero");
		}
	}

	private void cargarDesdeFichero(){
		try{
			File fichero = new File(nombreFichero);
			if (fichero.createNewFile()) {
				System.out.println("Acaba de crearse un nuevo fichero");
			} else {
				Scanner sc = new Scanner(fichero);
				while(sc.hasNext()){
					listaAviones.add(new Avion(sc.next(), sc.next(), sc.next(), sc.next(), sc.next()));
				}
			}
		}catch(IOException ex){
		}
	}



	public void borrarAvion(String matricula){

	for(int j = 0; j < listaAviones.size(); j++){
		Avion obj = listaAviones.get(j);
		if (obj.getMatricula().equals(matricula)){
			listaAviones.remove(j);
			volcarAFichero();
		 	break;
			}
		}

	}
	
	public void generarHojaDeCalculo(){
		try{
			FileWriter fw = new FileWriter(nombreFicheroCSV);
			fw.write("Matrícula Modelo Fabricante Velocidad Alcance\n");
			fw.write(this.toString());
			fw.close();
		}catch (IOException ex){
			System.err.println("Error al intentar escribir en fichero");
		}
		}

	public void mostrarHojaDeCalculo(){
		try{ String cmd = "soffice aviones.csv";
			Runtime.getRuntime().exec(cmd);
		} catch (IOException ioe) {
			System.out.println(ioe); 
		}
	}
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Avion avion : listaAviones) sb.append(avion + "\n");
		return sb.toString();
	}
}
			

		




