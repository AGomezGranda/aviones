/**
 * Copyright 2019 Álvaro Gómez and Gonzalo Paúl
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package dominio;

public class Avion{
	private String modelo;
	private String fabricante;
	private String velocidad;
	private String alcance;
	private String matricula;

	public void setMatricula(String matricula){
		this.matricula = matricula;
        }

	public String getMatricula(){
		return matricula;
        }

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setVelocidad(String velocidad){
		this.velocidad = velocidad;
	}

	public String getVelocidad() {
		return velocidad;
	}

	public void setAlcance(String alcance) {
		this.alcance = alcance;
	}

	public String getAlcance() {
		return alcance;
	}
	
	public Avion(String matricula, String modelo, String fabricante, String velocidad, String alcance){
		this.matricula = matricula;
		this.modelo = modelo;
		this.fabricante = fabricante;
		this.velocidad = velocidad;
		this.alcance = alcance;
	}


	public String generarStringParaMostrar(){
		return "Con matricula:" + getMatricula() + " el modelo " + getModelo() + " del fabricante " + getFabricante() + " alcanza una velocidad máxima de " + getVelocidad() + " km/h" + " y tiene un alcance de " + getAlcance() +" km ";
	}

	public String toString() {
		return getMatricula() + " " +  getModelo() + " " + getFabricante() + " " + getVelocidad() + " " + getAlcance();
	}
}	



