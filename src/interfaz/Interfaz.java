/**
 * Copyright 2019 Álvaro Gómez and Gonzalo Paúl
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package interfaz;

import dominio.Catalogo;
import dominio.Avion;
import java.lang.ArrayIndexOutOfBoundsException;

public class Interfaz{
    private static Catalogo catalogo = new Catalogo();

    private static void mostrarAyuda(){
        System.out.println("Las instrucciones posibles son las siguientes:");
        System.out.println("   1. Mostrar lista aviones: java -jar aviones.jar show");
        System.out.println("   2. Mostrar esta ayuda: java -jar aviones.jar help");
        System.out.println("   3. Añadir avión: java -jar aviones.jar add <matícula> <modelo> <fabricante> <velocidad máxima> <alcance>, por ejemplo, ");
	System.out.println("   4. Eliminar avión: java -jar aviones.jar remove <matrícula>");
	System.out.println("   5. Modificar registro avión: java -jar aviones.jar modify <matrícula> <modelo> <fabricante> <velocidad máxima> <alcance>");
	System.out.println("   6. Para generar la hoja de calculo del catalogo: java -jar aviones.jar spreadsheet");
        System.out.println("   7. Para ver la ver la hoja de calculo (a través de office): java -jar aviones.jar showSpreadsheet"); 	
    }

    public static void ejecutar(String[] args){
        try
        {
            if (args[0].equalsIgnoreCase("add"))
                catalogo.annadirAvion(new Avion(args[1], args[2], args[3], args[4], args[5]));
            else if (args[0].equalsIgnoreCase("show")){
                catalogo.mostrarAviones();
	    }
	    
	else if (args[0].equalsIgnoreCase("remove")){
		catalogo.borrarAvion(args[1]);
		}
	else if (args[0].equalsIgnoreCase("modify")){
		catalogo.borrarAvion(args[1]);
		catalogo.annadirAvion(new Avion(args[1], args[2], args[3], args[4], args[5]));
		}
	else if (args[0].equalsIgnoreCase("spreadsheet")){
	    catalogo.generarHojaDeCalculo();
	}
	else if (args[0].equalsIgnoreCase("showSpreadsheet")){
		catalogo.mostrarHojaDeCalculo();
	}	
	else if (args[0].equalsIgnoreCase("help")) mostrarAyuda();
            else mostrarAyuda();
        }catch(ArrayIndexOutOfBoundsException ex){
            mostrarAyuda();
        }
    }
}

